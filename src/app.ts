import express from 'express';
import rootRoute from './routes';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import { accessLogger, sanitizer } from './middlewares';

// Initializing express.
const app = express();

// Access logging.
app.use(accessLogger);

// Cookie parser.
app.use(cookieParser());

// Body parser.
app.use(bodyParser.urlencoded({ 
  extended: true 
}));
app.use(bodyParser.json());

// Sanitizer.
app.use(sanitizer);

// CORS.
app.use(cors({
  credentials: true
}));

// Adding main router.
app.use('/', rootRoute);

// Done.
export = app;