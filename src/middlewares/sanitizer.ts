import { Request, Response } from 'express';

function sanitize<T>(v: T): T {
  if (v instanceof Object) {
    for (var key in v) {
      if (/^\$/.test(key)) {
        delete v[key];
      }
    }
  }
  return v;
}

/** Sanitizes request body to prevent nosql injection. */
function x(req: Request, _: Response, next: Function) {
  // Sanitizing body.
  req.body = sanitize(req.body);

  // Sanitizing query.
  req.query = sanitize(req.query);

  // Sanitizing cookie.
  req.cookies = sanitize(req.cookies);

  // Done.
  next();
}

export default x;