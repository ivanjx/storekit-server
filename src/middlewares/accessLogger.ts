import { Request, Response } from 'express';
import { log } from '../logger';

/** Logs any access to the http server to stdout. */
function x(req: Request, _: Response, next: Function) {
  let address: string = req.headers['x-forwarded-for'] as string || req.connection.remoteAddress as string;
  let contentLength: number = 0;

  try {
    contentLength = parseInt(req.headers["content-length"] as string) || 0;
  } catch(err) {}

  log(`${address} ${req.method} (${contentLength} B): ${req.url}`);
  next();
}

export default x;