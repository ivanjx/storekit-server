import dateformat from 'dateformat';
import cluster from 'cluster';

function log(message: any, pid: string = 'master'): void {
  if(cluster.isWorker) {
    cluster.worker.send({
      pid: process.pid,
      message: message
    });
  } else {
    let dateStr = dateformat(
      new Date(),
      'yyyy-mm-dd HH:MM:ss'
    );

    if(typeof message !== 'string') {
      message = JSON.stringify(message);
    }

    console.log(`[${dateStr}][${pid}] ${message}`);
  }
}

function logError(error: Error, pid: string = 'master'): void {
  log(`${error.message}${'\n'}${error.stack}`, pid);
}

export { 
  log,
  logError
};