import mongoose, { Document, Schema } from 'mongoose';
import { ModelHelper } from '../helpers';

// Interfaces.
export interface IModel extends Document {
  username: string,
  passwordHash: string,
  level: number
}

// Models.
const userSchema = new Schema({
  username: String,
  passwordHash: String,
  level: Number
}, {
  collection: 'users'
});

export const Model = mongoose.model<IModel>('User', userSchema);

// CRUD operations.
export async function getById(id: string): Promise<IModel> {
  let result =
  await Model
  .findOne({
    _id: ModelHelper.convertStringToObjectId(id)
  })
  .exec();

  if(!result) {
    throw new Error('User not found');
  }

  // Done.
  return result;
}