import mongoose, { Document, Schema } from 'mongoose';
import { ModelHelper } from '../helpers';

// Interfaces.
export interface IModel extends Document {
  name: string,
  description: string
}

// Models.
const inventorySchema = new Schema({
  name: String,
  description: String
}, {
  collection: 'inventories'
});

export const Model = mongoose.model<IModel>('Inventory', inventorySchema);

// CRUD operations.
async function validateName(name: string) {
  // Getting by name.
  let result =
  await Model
  .findOne({
    name: name
  })
  .exec();

  if(result) {
    throw new Error('Inventory name already used');
  }
}

export async function create(data: IModel) {
  // Validating name.
  validateName(data.name);

  // Saving.
  await new Model(data).save();
}

export async function getById(id: string): Promise<IModel> {
  let result =
  await Model
  .findOne({
    _id: ModelHelper.convertStringToObjectId(id)
  })
  .exec();

  if(!result) {
    throw new Error('Inventory not found');
  }

  // Done.
  return result;
}

export async function list(filter: string): Promise<IModel[]> {
  let query = {} as any;

  if(filter) {
    let regex = new RegExp(filter, 'i');
    query.name = regex
  }

  // Listing.
  return await Model
  .find(query)
  .sort({
    name: 1
  })
  .exec();
}

export async function deleteById(id: string) {
  // Deleting.
  await Model
  .deleteOne({
    _id: ModelHelper.convertStringToObjectId(id)
  })
  .exec();
}