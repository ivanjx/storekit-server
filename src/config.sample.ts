export const serverConfig = {
  host: 'http://localhost:3000',
  port: 3000
};

export const dbConfig = {
  connString: 'mongodb://localhost/xtremeunlocknew',
  options: {
    poolSize: 100,
    keepAlive: true,
    useNewUrlParser: true,
  }
};