import { Router } from 'express';

// Routes.
const router = Router();

router.get(
  '/',
  (_, res) => {
    res.end('Hello world...');
  }
);

// Done.
export = router;