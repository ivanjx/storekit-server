import { Document, Types } from 'mongoose';

/** Converts Document into javascript object, adds id and removes _id. */
export function convertModelToJSON(model: Document): any {
  let result = model.toObject();
  result.id = result._id.toString();
  delete result._id;
  return result;
}

/** Converts array of Documents into javascript object, adds id and removes _id. */
export function convertModelsToJSON(models: Document[]): any[] {
  let result = [];

  for(let i = 0; i < models.length; ++i) {
    result.push(convertModelToJSON(models[i]));
  }

  return result;
}

/** Validates an id string and converts it to mongoose's object id. */
export function convertStringToObjectId(id: string): Types.ObjectId {
  return new Types.ObjectId(id);
}

/** Deletes null or empty fields from object. Useful for mongoose query building. */
export function deleteNullOrEmptyFields(data: any): any {
  for(let field in data) {
    if(data[field] == null || data[field] == undefined) { // If null or undefined.
      delete data[field];
    } else if(Object.keys(data[field]).length == 0 && data[field].constructor === Object) { // If an empty object.
      delete data[field];
    } else if(Array.isArray(data[field]) && data[field].length == 0) { // If is an empty array.
      delete data[field];
    } else if(data[field] === '') { // If is an empty string.
      delete data[field];
    }
  }

  return data;
}