import mongoose from 'mongoose';
import { log } from '../logger';
import { dbConfig } from '../config';

mongoose.set('useFindAndModify', false);

/** Initializes mongoose and connects to mongodb. This will automatically reconnect to mongodb if disconnected. */
export async function connectToMongo() {
  try {
    await mongoose.connect(
      dbConfig.connString,
      dbConfig.options
    );
  } catch(err) {}
}

mongoose.connection.on('connected', () => {
  log('Connected to mongodb');
});

mongoose.connection.on('disconnected', () => {
  log('Disconnected from mongodb. Reconnecting...');
  connectToMongo();
});