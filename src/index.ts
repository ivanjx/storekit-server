import cluster from 'cluster';
import os from 'os';
import http from 'http';
import app from './app';
import { log } from './logger';
import { MongoInitHelper } from './helpers';
import { serverConfig } from './config';

if(cluster.isMaster) {
  // Start as server.
  // Getting cpu count.
  let cpuCount = os.cpus().length || 1;
  cpuCount = cpuCount * 2;

  // Worker started event.
  cluster.on('online', (worker) => {
    log(`A worker started. PID: ${worker.process.pid}`);
  });

  // Worker stopped event.
  cluster.on('exit', (worker) => {
    log(`A worker stopped. PID: ${worker.process.pid}`);

    // Starting new worker.
    log('Starting new worker...');
    cluster.fork();
  });

  // Worker message event.
  cluster.on('message', (_, message, __) => {
    log(message.message, message.pid);
  });

  // Starting workers.
  log(`Starting ${cpuCount} workers...`);

  for(let i = 0; i < cpuCount; ++i) {
    cluster.fork();
  }
} else {
  // Start as worker.
  // Configuring mongoose.
  MongoInitHelper.connectToMongo();

  // Starting http server.
  const server = new http.Server(app);
  server.listen(
    serverConfig.port, 
    () => log(`Server is listening on port ${serverConfig.port} ...`)
  );
}